let js_warning = document.getElementById("js_warning");
js_warning.parentElement.removeChild(js_warning);

const str_type = Object.getPrototypeOf("");
const dict_type = Object.getPrototypeOf({});
const page_title = "Mastodon Thread Maker"


let txt_data = {};



function split_counter(txt) {
    // automatically created from python code (and reworked)
    var elts, res;
    // split the whole text along some fraction-delimiter like "\n\n1/\n" or "\n\n23/42\n"
    elts = txt.split(/\n\n\d+\/\d*\n/);
    var res = [], _pj_b = elts;
    var tmp_str;
    for (var i = 0, _pj_d = elts.length; (i < _pj_d); i += 1) {
        var e = elts[i];
        tmp_str = (((i+1).toString() + ": ") + e.length.toString())
        res.push(tmp_str);
    }

    res_txt = res.join(";&nbsp;&nbsp; ");

//     console.log(res_txt);
    return res_txt;
}


let ta = document.getElementById("main_textarea");
let overall = document.getElementById("overall");
let toot_distribution = document.getElementById("toot_distribution");
let debugfield = document.getElementById("debugfield");

function calc_toots() {


  var src_txt = ta.value
  var characterCount = ta.value.length;

  toot_distribution.innerHTML = split_counter(src_txt);
  overall.innerHTML = characterCount;


}


/*
 * data structure: nested dictionary with this key hierarchy-> language -> id  -> (attribute)
 *
 */

txt_data.en = {};
txt_data.de = {};
txt_data.en.debugfield = "test-string in English";
txt_data.de.debugfield = "test-string auf deutsch";

txt_data.en.lang_switcher_link = "A 文 (en)";
txt_data.de.lang_switcher_link = "A 文 (de)";


txt_data.en.ft_txt1 = "No cookies. No tracking. Functionality happens in <i>your</i> browser.<br> This is a volunteer project. No warranty!";
txt_data.de.ft_txt1 = "Keine Cookies. Kein Tracking. Funktionalität passiert <i>Deinem</i> Browser.<br> Das ist ein privates freiwilliges Projekt. Keine Gewährleistung!";

txt_data.en.ft_link_doc = "Show doc (again)";
txt_data.de.ft_link_doc = "Zeige Doku (nochmal)";

txt_data.en.ft_link_repo = "source repo on codeberg";
txt_data.de.ft_link_repo = "Quellcode auf Codeberg";

txt_data.en.ft_link_legalnotice = "legal notice";
txt_data.de.ft_link_legalnotice = "Impressum";

txt_data.en.ft_link_author = "author";
txt_data.de.ft_link_author = "Author";

txt_data.en.ft_hosted_on = "<br>hosted on";
txt_data.de.ft_hosted_on = "<br>Gehostet auf";

txt_data.en.main_textarea = {"placeholder": "Type your toot thread here ..."};
txt_data.de.main_textarea = {"placeholder": "Tippe hier deinen Tröt-Thread..."};

txt_data.en._content = `                               %%
I want to explain something to you.              %%
Something not so trivial.       %%
Probably I will need quite a few words. More than will fit into a single toot.
So it will be a thread of several toots.
Because a toot typically can't have more than 500 characters^[1], I'm starting to have to think about where the first toot ends and the next one begins.

1/

This is annoying.        %%
Actually I want to write down a thought first and then package it into toot-able snippets.

The problem is: How do I find out where one snippet ends and the next one begins?

2/

This is where "Mastodon Thread Maker" (this WebApp here) helps. It is actually just a bunch of amateur JavaScript code (written by a Pythonista), which splits the whole text at each keystroke into blocks and writes the length of each block separately below the text.             %%

Take a look there.         %%
So far, there are three blocks.         %%
But a fourth is about to be added.     %%
Namely, when a line begins with "number-slash" and there is at least one empty line before and after it.
Countdown: %%
5 %%
4 %%
3 %%
2 %%
1 %%

3/

And that's it.
Now I have explained everything.
Now you can start yourself.
Just write in this textarea and from time to time take look below it.

When you are done, you can simply copy the sections and paste them into Mastodon. Currently, it doesn't get any more comfortable than this. Sorry.

4/

Oh, what I wanted to say:

[Footnote 1]: The 500 characters thing is not quite right. URLs e.g. are counted only up to a length of 23 characters. Everything above that is "free". But this behavior of Mastodon is not implemented here yet. I.e. URLs are counted wrong. Probably, there are also bugs in the code. If you notice something, please open an issue or contact the author. See the page footer for details.

5/

Now I have to finish for now. The page was just called by another person. I have to explain the same now again 🙄. (If you feel like replacing me at the keyboard - that would be great. My hands are starting to hurt... 😉)

Contact option in the footer.

6/6
`


/*
 *
 * visual delimieter, to help navigation as this file is getting longer.
 *
 */



txt_data.de._content = `                               %%
Ich möchte Dir was erklären.              %%
Und das ist nicht ganz trivial.       %%
Vermutlich werde ich ein paar mehr Worte brauchen. Mehr als in einen einzelnen Tröt passen.
Es wird also ein Thread aus mehreren Tröts.
Weil ein Tröt typischerweise nicht mehr als 500 Zeichen^[1] haben kann, muss ich mir langsam schon Gedanken machen, wo der erste Tröt aufhört und der nächste beginnt.

1/

Das nervt aber.        %%
Eigentlich will ich einen Gedanken erstmal aufschreiben und ihn danach in trötbare Schnipsel verpacken.

Das Problem dabei: Wie kriege ich raus, wo sinnvollerweise ein Tröt aufhöhrt und wo der nächste beginnt?

2/

Dabei hilft der "Mastodon Thread Maker" (also diese WebApp hier). Das ist eigentlich nur billiger Anfänger-JavaScript-Code, der bei jedem Tastenanschlag den gesamten Text an "Tröt-Trennern" in Blöcke aufspaltet und für jeden Block separat die Länge unter den Text schreibt.             %%

Guck mal da hin.         %%
Bisher gibt es drei Blöcke. Aber gleich kommt ein vierter hinzu.     %%
Nämlich dann, wenn eine Zeile mit "Zahl-Slash" beginnt und es mindestens je eine Leerzeilden davor und danach gibt.
Countdown:           %%
5           %%
4           %%
3           %%
2           %%
1           %%

3/

So. Das ist es auch schon.
Jetzt habe ich eigentlich alles erklärt.
Jetzt kannst Du selber loslegen.
Einfach hier rein schreiben und gelegentlich unter das Textfeld schauen.

Wenn Du fertig bist, kannst Du dann die einzelnen Abschnitte einfach kopieren und bei Mastodon einfügen. Bequemer geht es gerade noch nicht. Sorry.

4/

Achso, was ich noch sagen wollte:

[Fußnote 1]: Das mit den 500 Zeichen ist nicht ganz richtig. URLs z.B. werden nur bis zu einer Länge von 23 Zeichen gezählt. Alles darüber gibt es "gratis". Dieses Verhalten von Mastodon ist hier aber noch nicht implementiert. D.h. URLs werden falsch gezählt. Außerdem sind bestimmt auch sonst noch Bugs im Code. Wenn Dir was auffällt, öffne am besten eine Issue. URL steht unten.

5/

Jetzt muss ich erstmal Schluss machen. Die Seite wurde gerade von einer weiteren Person aufgerufen. Der muss ich das gleiche jetzt auch noch mal erklären 🙄. (Falls Du Lust hast, mich als Erklärbär an der Tastatur abzulösen – das wäre echt super. Mir tun langsam die Hände weh... 😉)

Kontaktmöglichkeit in der Fußzeile.

6/6`;


var i = 0;  // counter for typeWriter. must be global


function get_parameter_from_url(name){
    // source: https://stackoverflow.com/a/831060
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}


function select_language(){

    valid_languages = Object.keys(txt_data);

    let lang = get_parameter_from_url("lang");
    source = "url";
    if (lang == undefined) {
        lang = navigator.language;
        source = "navigator default";
    }

    console.log("identified lang:", lang, "("+source+")");

    if (!valid_languages.includes(lang)) {
        console.log("language not supported. switching to 'en'");
        lang = "en";
    }

    return lang;

}

function typeWriter() {
    if (i == 0) {
        ta.value = "";
    }
    if (i < txt.length) {
        ta.value += txt.charAt(i);

        // scroll to bottom
        ta.scrollTop = ta.scrollHeight
        i++;
        calc_toots();

    }
    else
    {
        // prevent further calling of this function
        clearInterval(intervalId);
        i=0;
    }
    console.log(i);

}

function stop_animation(){

    ta.value = "";
    overall.innerHTML = 0;
    clearInterval(intervalId);
    ta.removeEventListener('keydown', stop_animation);

}


function show_doc(){

    clearInterval(intervalId);
    ta.removeEventListener('keydown', stop_animation);
    ta.value = txt;
    calc_toots();

}

function set_all_language_strings(dict){
    /*
     * dict is expected to be a dictionary (i.e. map?)
     *
     * key: id; value either str -> set innerHTML or another dict -> set attribute
     */

    let items = Object.entries(dict);
    let j = 0;
    for (let j = 0; (j < items.length); j += 1) {

        let [k, v] = items[j];
        set_language_string(k, v);

    }
}

function set_language_string(id_str, value){

    if (id_str.startsWith("_")) {
        // noting to do by convention
        return 0
    }

    let elt = document.getElementById(id_str);
    if (elt == undefined) {
        console.log(id_str, "not found while setting language string")
        return -1
    }

    if (Object.getPrototypeOf(value) == str_type){

        elt.innerHTML = value;
        return 0
    }
    else if (Object.getPrototypeOf(value) == dict_type){

        let items = Object.entries(value);
        for (let j2 = 0; (j2 < items.length); j2 += 1) {

            let [k, v] = items[j2];

            elt[k] = v;
        }
        return 0

    }
    else {

        console.log("unexpected Prototype (neiher string nor dict):", Object.getPrototypeOf(value));
        return -1

    }

}

function toggle_ls_menu(){

    let elt = document.getElementById("lang_switcher_dropdown_content");


    if ((elt.style["display"] == "none") || (elt.style["display"] == ""))
    {
        elt.style["display"] = "block";
    }
    else
    {
        elt.style["display"] = "none";
    }
}

function apply_lang_code(lang_code1) {

    txt_dict = txt_data[lang_code1];
    set_all_language_strings(txt_dict);
    txt = txt_dict._content.replaceAll(" %%\n", "  \n");

    //TODO make this more robust for multiple language changes in a row
    if (window.history.replaceState && get_parameter_from_url("lang") == undefined) {

        url = window.location.href + "?lang=" + lang_code1;
        //prevents browser from storing history with each change:
        window.history.replaceState({}, page_title, url);
    }


    // now we want to close the menu if we are in touch screen mode
    // (which must first be determined)
    let elt = document.getElementById("lang_switcher_link");
    let s = getComputedStyle(elt);

    if (s["pointer-events"] == "none")
    {
        // == "none" means we are in "hover mode" (menu is shown on hover, not on click
        // -> nothing todo
        //console.log("dbg1: hover", s["pointer-events"]);

    }
    else
    {
        // we are in "touch screen mode":
        // close the menu on click;
        //console.log("dbg1: touch", s["pointer-events"]);
        document.getElementById("lang_switcher_dropdown_content").style["display"] = "none";

    }



}


function debug(){
    stop_animation();
    debugfield.style["display"] = "block";

    //debugfield.innerHTML = txt_data["en"];
    return "done"

}

ta.addEventListener('keydown', stop_animation);
ta.addEventListener('keyup', calc_toots);

lang_code = select_language();
var txt_dict = {};
apply_lang_code(lang_code)

// prepare data for the type writer
txt = txt_dict._content.replaceAll(" %%\n", "  \n");
var sleep_time = 50;

ta.focus();
var intervalId = setInterval(typeWriter, sleep_time);

//debug();




